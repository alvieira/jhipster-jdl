import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JdlDepartmentModule } from './department/department.module';
import { JdlJobHistoryModule } from './job-history/job-history.module';
import { JdlJobModule } from './job/job.module';
import { JdlEmployeeModule } from './employee/employee.module';
import { JdlLocationModule } from './location/location.module';
import { JdlTaskModule } from './task/task.module';
import { JdlCountryModule } from './country/country.module';
import { JdlRegionModule } from './region/region.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        JdlDepartmentModule,
        JdlJobHistoryModule,
        JdlJobModule,
        JdlEmployeeModule,
        JdlLocationModule,
        JdlTaskModule,
        JdlCountryModule,
        JdlRegionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JdlEntityModule {}
