import { NgModule } from '@angular/core';

import { JdlSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [JdlSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [JdlSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class JdlSharedCommonModule {}
