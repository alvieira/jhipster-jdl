import { Moment } from 'moment';

export interface IJobHistory {
    id?: number;
    startDate?: Moment;
    endDate?: Moment;
    departmentId?: number;
    jobId?: number;
    employeeId?: number;
}

export class JobHistory implements IJobHistory {
    constructor(
        public id?: number,
        public startDate?: Moment,
        public endDate?: Moment,
        public departmentId?: number,
        public jobId?: number,
        public employeeId?: number
    ) {}
}
