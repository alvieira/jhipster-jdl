export interface ICountry {
    id?: number;
    countryId?: number;
    countryName?: string;
    regionId?: number;
}

export class Country implements ICountry {
    constructor(public id?: number, public countryId?: number, public countryName?: string, public regionId?: number) {}
}
