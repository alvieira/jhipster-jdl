import { IEmployee } from 'app/shared/model//employee.model';

export interface IDepartment {
    id?: number;
    departmentId?: number;
    departmentName?: string;
    locationId?: number;
    employees?: IEmployee[];
}

export class Department implements IDepartment {
    constructor(
        public id?: number,
        public departmentId?: number,
        public departmentName?: string,
        public locationId?: number,
        public employees?: IEmployee[]
    ) {}
}
