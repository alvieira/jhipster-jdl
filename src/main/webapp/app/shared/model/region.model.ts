export interface IRegion {
    id?: number;
    regionId?: number;
    regionName?: string;
}

export class Region implements IRegion {
    constructor(public id?: number, public regionId?: number, public regionName?: string) {}
}
