export interface ILocation {
    id?: number;
    locationId?: number;
    streetAddress?: string;
    postalCode?: string;
    city?: string;
    stateProvince?: string;
    countryId?: number;
}

export class Location implements ILocation {
    constructor(
        public id?: number,
        public locationId?: number,
        public streetAddress?: string,
        public postalCode?: string,
        public city?: string,
        public stateProvince?: string,
        public countryId?: number
    ) {}
}
