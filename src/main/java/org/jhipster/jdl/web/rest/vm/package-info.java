/**
 * View Models used by Spring MVC REST controllers.
 */
package org.jhipster.jdl.web.rest.vm;
